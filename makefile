all: test

test: test_main.o test_table.o
	g++ -g -O0 -std=c++20 -o $@ $^

%.o: %.cpp
	g++ -g -Werror -O0 -std=c++20 -o $@ -c $<

test_table.o: test_main.cpp hash_table.hpp

.PHONY: clean run

clean:
	rm *.o test

run:
	./test
