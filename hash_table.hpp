#include <string>
#include <memory>
#include <list>
#include <optional>
#include <functional>

template <typename K, typename V>
class hash_table
{
    private:
        struct node
        {
            K key;
            V value;
            bool operator==(const K &k)
            {
                return this->key == k;
            }
        };

        using list = std::list<node>;
        using array = std::unique_ptr<list[]>;
        std::size_t capacity;
        std::size_t size;
        array table;

        void rehashing();

    public:
        hash_table();
        ~hash_table() = default;

        void insert(const K &key, const V value);
        std::optional<V> get(const K &key);
        void remove(const K &key);
};

template <typename K, typename V>
hash_table<K,V>::hash_table() :
    capacity(1000),
    table(std::make_unique<list[]>(capacity)),
    size(0)
{}

template <typename K, typename V>
void hash_table<K,V>::insert(const K &key, const V value)
{
    auto index = std::hash<K>{}(key) % capacity;

    auto it = std::find(table[index].begin(), table[index].end(), key);
    if (it != table[index].end())
    {
        it->value = value;
        return;
    }

    table[index].emplace_back(key, value);

    if (++size > capacity / 2)
        rehashing();
}

template <typename K, typename V>
std::optional<V> hash_table<K,V>::get(const K &key)
{
    auto index = std::hash<K>{}(key) % capacity;
    auto it = std::find(table[index].begin(), table[index].end(), key);

    if (it == table[index].end())
        return std::nullopt;
    else
        return it->value;
}

template <typename K, typename V>
void hash_table<K,V>::remove(const K &key)
{
    auto index = std::hash<K>{}(key) % capacity;
    auto it = std::find(table[index].begin(), table[index].end(), key);
    if (it != table[index].end())
        table[index].erase(it);
}

template <typename K, typename V>
void hash_table<K,V>::rehashing()
{
    auto new_capacity = capacity * 2;
    auto new_table = std::make_unique<list[]>(new_capacity);

    for (std::size_t chain = 0; chain < capacity; ++chain)
    {
        std::for_each(table[chain].cbegin(), table[chain].cend(), [&](auto &node) {
                new_table[std::hash<K>{}(node.key) % new_capacity].emplace_back(std::move(node));
        });
    }

    capacity = new_capacity;
    table.swap(new_table);
}
